/****************************************************************************
** Meta object code from reading C++ file 'frmipc.h'
**
** Created: Mon Oct 22 13:40:13 2018
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../frmipc.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'frmipc.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_frmIPC[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
       8,    7,    7,    7, 0x08,
      28,    7,    7,    7, 0x08,
      51,    7,    7,    7, 0x08,
      74,    7,    7,    7, 0x08,
     102,   96,    7,    7, 0x08,
     141,  136,    7,    7, 0x08,
     173,  136,    7,    7, 0x08,
     216,  136,    7,    7, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_frmIPC[] = {
    "frmIPC\0\0on_btnAdd_clicked()\0"
    "on_btnDelete_clicked()\0on_btnUpdate_clicked()\0"
    "on_btnExcel_clicked()\0index\0"
    "on_tableMain_pressed(QModelIndex)\0"
    "arg1\0on_cboxNVRID_activated(QString)\0"
    "on_txtIPCRtspAddrMain_textChanged(QString)\0"
    "on_cboxIPCID_activated(QString)\0"
};

void frmIPC::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        frmIPC *_t = static_cast<frmIPC *>(_o);
        switch (_id) {
        case 0: _t->on_btnAdd_clicked(); break;
        case 1: _t->on_btnDelete_clicked(); break;
        case 2: _t->on_btnUpdate_clicked(); break;
        case 3: _t->on_btnExcel_clicked(); break;
        case 4: _t->on_tableMain_pressed((*reinterpret_cast< const QModelIndex(*)>(_a[1]))); break;
        case 5: _t->on_cboxNVRID_activated((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 6: _t->on_txtIPCRtspAddrMain_textChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 7: _t->on_cboxIPCID_activated((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData frmIPC::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject frmIPC::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_frmIPC,
      qt_meta_data_frmIPC, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &frmIPC::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *frmIPC::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *frmIPC::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_frmIPC))
        return static_cast<void*>(const_cast< frmIPC*>(this));
    return QDialog::qt_metacast(_clname);
}

int frmIPC::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
