/****************************************************************************
** Meta object code from reading C++ file 'frmpollconfig.h'
**
** Created: Mon Oct 22 13:40:14 2018
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../frmpollconfig.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'frmpollconfig.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_frmPollConfig[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       7,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      15,   14,   14,   14, 0x08,
      38,   14,   14,   14, 0x08,
      61,   14,   14,   14, 0x08,
      87,   14,   14,   14, 0x08,
     113,   14,   14,   14, 0x08,
     141,  135,   14,   14, 0x08,
     180,  135,   14,   14, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_frmPollConfig[] = {
    "frmPollConfig\0\0on_btnAddOne_clicked()\0"
    "on_btnAddAll_clicked()\0on_btnRemoveOne_clicked()\0"
    "on_btnRemoveAll_clicked()\0"
    "on_btnExcel_clicked()\0index\0"
    "on_treeMain_doubleClicked(QModelIndex)\0"
    "on_tableMain_doubleClicked(QModelIndex)\0"
};

void frmPollConfig::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        frmPollConfig *_t = static_cast<frmPollConfig *>(_o);
        switch (_id) {
        case 0: _t->on_btnAddOne_clicked(); break;
        case 1: _t->on_btnAddAll_clicked(); break;
        case 2: _t->on_btnRemoveOne_clicked(); break;
        case 3: _t->on_btnRemoveAll_clicked(); break;
        case 4: _t->on_btnExcel_clicked(); break;
        case 5: _t->on_treeMain_doubleClicked((*reinterpret_cast< const QModelIndex(*)>(_a[1]))); break;
        case 6: _t->on_tableMain_doubleClicked((*reinterpret_cast< const QModelIndex(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData frmPollConfig::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject frmPollConfig::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_frmPollConfig,
      qt_meta_data_frmPollConfig, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &frmPollConfig::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *frmPollConfig::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *frmPollConfig::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_frmPollConfig))
        return static_cast<void*>(const_cast< frmPollConfig*>(this));
    return QDialog::qt_metacast(_clname);
}

int frmPollConfig::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 7)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
