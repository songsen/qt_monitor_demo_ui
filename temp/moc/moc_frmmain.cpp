/****************************************************************************
** Meta object code from reading C++ file 'frmmain.h'
**
** Created: Mon Oct 22 13:40:12 2018
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../frmmain.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'frmmain.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_frmMain[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      14,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
       9,    8,    8,    8, 0x08,
      24,    8,    8,    8, 0x08,
      38,    8,    8,    8, 0x08,
      54,    8,    8,    8, 0x08,
      73,    8,    8,    8, 0x08,
      92,    8,    8,    8, 0x08,
     113,    8,    8,    8, 0x08,
     134,    8,    8,    8, 0x08,
     149,    8,    8,    8, 0x08,
     164,    8,    8,    8, 0x08,
     179,    8,    8,    8, 0x08,
     195,    8,    8,    8, 0x08,
     222,    8,    8,    8, 0x08,
     253,  247,    8,    8, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_frmMain[] = {
    "frmMain\0\0change_style()\0screen_full()\0"
    "screen_normal()\0delete_video_one()\0"
    "delete_video_all()\0snapshot_video_one()\0"
    "snapshot_video_all()\0show_video_1()\0"
    "show_video_4()\0show_video_9()\0"
    "show_video_16()\0on_btnMenu_Close_clicked()\0"
    "on_btnMenu_Min_clicked()\0index\0"
    "on_treeMain_doubleClicked(QModelIndex)\0"
};

void frmMain::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        frmMain *_t = static_cast<frmMain *>(_o);
        switch (_id) {
        case 0: _t->change_style(); break;
        case 1: _t->screen_full(); break;
        case 2: _t->screen_normal(); break;
        case 3: _t->delete_video_one(); break;
        case 4: _t->delete_video_all(); break;
        case 5: _t->snapshot_video_one(); break;
        case 6: _t->snapshot_video_all(); break;
        case 7: _t->show_video_1(); break;
        case 8: _t->show_video_4(); break;
        case 9: _t->show_video_9(); break;
        case 10: _t->show_video_16(); break;
        case 11: _t->on_btnMenu_Close_clicked(); break;
        case 12: _t->on_btnMenu_Min_clicked(); break;
        case 13: _t->on_treeMain_doubleClicked((*reinterpret_cast< const QModelIndex(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData frmMain::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject frmMain::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_frmMain,
      qt_meta_data_frmMain, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &frmMain::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *frmMain::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *frmMain::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_frmMain))
        return static_cast<void*>(const_cast< frmMain*>(this));
    return QDialog::qt_metacast(_clname);
}

int frmMain::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 14)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 14;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
