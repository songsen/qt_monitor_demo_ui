/****************************************************************************
** Meta object code from reading C++ file 'frmconfig.h'
**
** Created: Mon Oct 22 13:40:13 2018
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../frmconfig.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'frmconfig.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_frmConfig[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      11,   10,   10,   10, 0x08,
      30,   10,   10,   10, 0x08,
      55,   10,   10,   10, 0x08,
      80,   10,   10,   10, 0x08,
     106,   10,   10,   10, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_frmConfig[] = {
    "frmConfig\0\0on_btnOk_clicked()\0"
    "on_btnClearNVR_clicked()\0"
    "on_btnClearIPC_clicked()\0"
    "on_btnClearPoll_clicked()\0"
    "on_btnClearAll_clicked()\0"
};

void frmConfig::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        frmConfig *_t = static_cast<frmConfig *>(_o);
        switch (_id) {
        case 0: _t->on_btnOk_clicked(); break;
        case 1: _t->on_btnClearNVR_clicked(); break;
        case 2: _t->on_btnClearIPC_clicked(); break;
        case 3: _t->on_btnClearPoll_clicked(); break;
        case 4: _t->on_btnClearAll_clicked(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData frmConfig::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject frmConfig::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_frmConfig,
      qt_meta_data_frmConfig, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &frmConfig::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *frmConfig::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *frmConfig::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_frmConfig))
        return static_cast<void*>(const_cast< frmConfig*>(this));
    return QDialog::qt_metacast(_clname);
}

int frmConfig::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
